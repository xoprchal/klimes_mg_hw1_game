#include <cstdlib>
#include <iostream>
#include <random>
#include <ctime>
#include "score.hpp"

const int high_bound = 100;

int main(int argc, char *argv[]) {
	std::srand(std::time(nullptr));
	int number_of_guesses = 0;
	int guess;
	int number = std::rand() % high_bound;
	std::cout << "I am thinking of number between 0 and " << high_bound << "\n";

	do {
		std::cout << "Guess a number: ";
		std::cin >> guess;
		if (number == guess) {
			std::cout << "Yes! I was thinking " << guess << '\n';
		} else {
			std::cout << "No, that's not quite it, my number was " << (number > guess ? "higher" : "lower") << '\n';
		}
		number_of_guesses++;

	} while (guess != number);

	std::string name;
	std::cout << "What is your name?:";
	std::cin >> name;

	do_score(number_of_guesses, name);


	return 0;
}
