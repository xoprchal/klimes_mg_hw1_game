#ifndef SCORE_H
#define SCORE_H
#include <iostream>
#include <filesystem>
#include <fstream>
#include <utility>
#include <vector>

void do_score(int new_score, const std::string& new_name) {
    bool written = false;
    std::vector<std::pair<int, std::string>> scoreObject;
    std::ifstream scorefile ("score.txt");
    if (!scorefile.is_open()) {
        std::cout << "Could not open score file, creating new.\n";

        std::cout << new_score << "\tname:" << new_name  << "\n";
        scoreObject.push_back(std::make_pair(new_score, new_name));
        written = true;
    std::ofstream scorewfile("score.txt", std::ios::out);
    for (const auto& o : scoreObject){
        scorewfile << o.first << '\t' << o.second << '\n';
    }
    scorewfile.close();

        return;
    }

    std::cout << "Scoreboard:\n";

    while (!scorefile.eof()) {
    int score;
    std::string name;
    scorefile >> score;
    getline(scorefile, name);

    if (new_score > score) {
        std::cout << new_score << "\tname:" << new_name  << "\n";
        scoreObject.push_back(std::make_pair(new_score, new_name));
        written = true;

    }

        std::cout << score << "\tname:" << name  << "\n";
    scoreObject.push_back(std::make_pair(score, name));

    }
    if (!written) {
        std::cout << new_score << "\tname:" << new_name  << "\n";
        scoreObject.push_back(std::make_pair(new_score, new_name));
        written = true;

    }


    scorefile.close();

    std::ofstream scorewfile("score.txt", std::ios::out);
    for (const auto& o : scoreObject){
        scorewfile << o.first << '\t' << o.second << '\n';
    }
    scorewfile.close();


}


#endif // SCORE_H
